# 饿汉式 
### 1. 静态常量饿汉式 HungrySingleton
### 2. 静态代码块饿汉式 HungryStaticSingleton

# 懒汉式
### 1. 静态变量饿汉式 LazySingleton
### 2. 静态代码块饿汉式 LazyStaticSingleton
### 3. 同步方法饿汉式  LazySyncSingleton
### 4. 双锁机制饿汉式 LazyDoubleCheckSingleton
### 5. 内部类饿汉式 LazyInnerClassSingleton
### 6. 注册式饿汉式 ContainerSingleton
### 7. ThreadLocal饿汉式 ThreadLocalSingleton
### 8. 枚举类饿汉式  EnumSingleton 



