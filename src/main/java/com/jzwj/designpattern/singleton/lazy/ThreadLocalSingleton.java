package com.jzwj.designpattern.singleton.lazy;

/**
 * @ClassName ThreadLocalSingleton
 * @Description ThreadLocal饿汉式
 * 优点：其实是伪线程安全的，多线程使用中，线程中各自线程中是唯一的，但是各个线程之间是互不一样的。
 * 缺点：正由于线程中各自线程中是唯一的，但是各个线程之间是互不一样的。所以单线程是安全的，具体使用场景有限，适用于ORM框架中实现多数据源动态切换等
 * @Author jiangZhiWen
 * @Date 2021/6/26 0:45
 * @Version 1.0
 */
public class ThreadLocalSingleton {
    private ThreadLocalSingleton() {
    }

    private static final ThreadLocal<ThreadLocalSingleton> singleton =
            new ThreadLocal<ThreadLocalSingleton>() {
                @Override
                protected ThreadLocalSingleton initialValue() {
                    return new ThreadLocalSingleton();
                }
            };

    public static ThreadLocalSingleton getInstance() {
        return singleton.get();
    }

}