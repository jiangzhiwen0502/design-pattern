package com.jzwj.designpattern.singleton.lazy;

/**
 * @ClassName EnumSingleton
 * @Description 枚举类饿汉式 ：
 * 优点：通过 Java 枚举类型本身的特性，保证了实例创建的线程安全性和实例的唯一性。原因：在 Java 规范中规定了每个枚举类型及其定义的枚举变量在 JVM 中都必须是唯一的，
 * 因此在枚举对象的序列化仅仅是将枚举对象的属性输出到结果中，反序列化的时候则是通过 valueOf 方法来查找枚举对象。枚举式单例之所以能成为最优雅的一种写法，原因就是 JDK 底层已经帮我们保证了不允许反射，也确保了序列化方式获得的对象仍然唯一
 * @Author jiangZhiWen
 * @Date 2021/6/26 0:50
 * @Version 1.0
 */
public enum EnumSingleton {
    INSTANCE;

    private MyObject myObject;

    EnumSingleton() {
        this.myObject = new MyObject();
    }

    public Object getData() {
        return myObject;
    }

    public static EnumSingleton getInstance(){
        return INSTANCE;
    }
}
class MyObject {
}