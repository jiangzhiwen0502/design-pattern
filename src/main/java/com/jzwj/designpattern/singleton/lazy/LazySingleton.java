package com.jzwj.designpattern.singleton.lazy;

/**
 * @ClassName LazySingleton
 * @Description 静态变量饿汉式
 * 优点：这种写法起到了Lazy Loading的效果，但是只能在单线程下使用
 * 缺点：线程不安全，如果在多线程下，一个线程进入了if (null == lazySingleton)判断语句块，还未来得及往下执行，另一个线程也通过了这个判断语句，这时便会产生多个实例。所以在多线程环境下不可使用这种方式。
 * @Author jiangZhiWen
 * @Date 2021/6/25 23:41
 * @Version 1.0
 */
public class LazySingleton {
    private static LazySingleton lazySingleton = null;

    private LazySingleton() {
    }

    public static LazySingleton getInstance() {
        if (null == lazySingleton) {
            //为空则说明第一次获取单例对象，进行初始化
            lazySingleton = new LazySingleton();
        }
        //不为空则说明已经初始化了，直接返回
        return lazySingleton;
    }
}