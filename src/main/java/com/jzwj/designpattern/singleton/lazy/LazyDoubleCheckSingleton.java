package com.jzwj.designpattern.singleton.lazy;

/**
 * @ClassName LazyDoubleCheckSingleton
 * @Description 双锁机制饿汉式：双重检查锁（double-checked locking） 除了两层if语句 还在静态变量lazySingleton上加入了 volatile 关键字
 * 优点：volatile 解决多线程下的可见性问题，synchronized 关键字移到了方法内部，尽可能缩小加锁的代码块，提升效率
 * 缺点：从源码可知，代码自动优化问题仍然存在着指令重排的情况，可能会抛出错误
 *
 * @Author jiangZhiWen
 * @Date 2021/6/26 0:02
 * @Version 1.0
 */
public class LazyDoubleCheckSingleton {
    private volatile static LazyDoubleCheckSingleton lazySingleton = null;

    private LazyDoubleCheckSingleton() {
    }

    public static LazyDoubleCheckSingleton getInstance(){
        if(null == lazySingleton){
            synchronized (LazyDoubleCheckSingleton.class){
                if(null == lazySingleton){
                    lazySingleton = new LazyDoubleCheckSingleton();
                }
            }
        }
        return lazySingleton;
    }
}