package com.jzwj.designpattern.singleton.lazy;

/**
 * @ClassName ContainerSingleton
 * @Description 注册式单例：参考Spring对单例的底层简单实现，将每一个实例都保存起来，然后在需要使用的时候直接通过唯一的标识获取实例。ioc 加入了 synchronized 关键字
 * 优点：饿汉和懒汉都有一个缺点，单例不能被继承。因为构造函数是私有的。而单例注册表的构造函数是protected。可以继承，spring中名称和实例一对一关系。hashmap就很适合这样的场景。spring是用的currenthashmap。即保证了线程安全又满足了性能需求
 * 缺点：难以理解
 * @Author jiangZhiWen
 * @Date 2021/6/26 0:33
 * @Version 1.0
 */

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ContainerSingleton {
    private ContainerSingleton() {
    }

    //存储单例对象
    private static Map<String, Object> ioc = new ConcurrentHashMap<>();

    public static Object getBean(String className) {
        synchronized (ioc) {
            //如果容器中不存在当前对象
            if (!ioc.containsKey(className)) {
                Object obj = null;
                try {
                    obj = Class.forName(className).newInstance();
                    //将className作为唯一标识存入容器
                    ioc.put(className, obj);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return obj;
            }
            //如果容器中已经存在了单例对象，则直接返回
            return ioc.get(className);
        }
    }
}