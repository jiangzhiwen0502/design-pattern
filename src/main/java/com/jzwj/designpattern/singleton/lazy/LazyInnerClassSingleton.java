package com.jzwj.designpattern.singleton.lazy;

import java.io.Serializable;

/**
 * @ClassName LazyInnerClassSingleton
 * @Description 这种方式跟饿汉式方式采用的机制类似，但又有所不同
 * 优点:两者都是采用了类装载的机制来保证初始化实例时只有一个线程,不同的地方在饿汉式方式是只要Singleton类被装载就会实例化，没有Lazy-Loading的作用，
 * 而静态内部类方式在Singleton类被装载时并不会立即实例化，而是在需要实例化时，调用getInstance方法，才会装载SingletonInstance类，从而完成Singleton的实例化,
 * 类的静态属性只会在第一次加载类的时候初始化，所以在这里，JVM帮助我们保证了线程的安全性，在类进行初始化时，别的线程是无法进入的。推荐使用
 * @Author jiangZhiWen
 * @Date 2021/6/26 0:16
 * @Version 1.0
 */
public class LazyInnerClassSingleton  {
    private LazyInnerClassSingleton() {
        //防止反射破坏单例
        if(null != InnerLazy.LAZY){
            throw new RuntimeException("不允许通过反射类构造单例对象");
        }
    }

    public static final LazyInnerClassSingleton getInstance() {
        return InnerLazy.LAZY;
    }

    private static class InnerLazy {
        private static final LazyInnerClassSingleton LAZY = new LazyInnerClassSingleton();
    }
}

/**
 *  不允许反射破坏的内部类懒汉单例，拦截住构造方法
 */
class LazyInnerClassSingletonPro {
    private LazyInnerClassSingletonPro() {
        //防止反射破坏单例
        if(null != LazyInnerClassSingletonPro.InnerLazy.LAZY){
            throw new RuntimeException("不允许通过反射类构造单例对象");
        }
    }
    public static final LazyInnerClassSingletonPro getInstance() {
        return InnerLazy.LAZY;
    }

    private static class InnerLazy {
        private static final LazyInnerClassSingletonPro LAZY = new LazyInnerClassSingletonPro();
    }
}


class LazyInnerClassSingletonPlus implements Serializable {
    private LazyInnerClassSingletonPlus() {
        //防止反射破坏单例
        if(null != InnerLazy.LAZY){
            throw new RuntimeException("不允许通过反射类构造单例对象");
        }
    }

    public static final LazyInnerClassSingletonPlus getInstance() {
        return InnerLazy.LAZY;
    }

    private static class InnerLazy {
        private static final LazyInnerClassSingletonPlus LAZY = new LazyInnerClassSingletonPlus();
    }
    /**
     * 序列化把java对象 保存到本地 再加载到内存 仍然会破坏单例模式,为了不允许序列化破坏的内部类懒汉单例，在单例类的代码加入 readResolve() 方法就行
     * JDK 源码中在序列化的时候会检验一个类中是否存在一个 readResolve 方法，
     * 如果存在，则会放弃通过序列化产生的对象，而返回原本的对象。
     * @return
     */
    private Object readResolve(){
        return InnerLazy.LAZY;
    }
}