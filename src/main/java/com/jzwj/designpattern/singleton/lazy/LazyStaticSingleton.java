package com.jzwj.designpattern.singleton.lazy;

/**
 * @ClassName LazyStaticSingleton
 * @Description 静态代码块饿汉式
 * 优点：由于 LazySingleton 懒汉式 实现方式同步效率太低，所以摒弃同步方法，改为同步产生实例化的的代码块。
 * 缺点：和不能起到线程同步的作用。
 * @Author jiangZhiWen
 * @Date 2021/6/25 23:51
 * @Version 1.0
 */
public class LazyStaticSingleton {

    private static LazyStaticSingleton singleton;

    private LazyStaticSingleton() {
    }

    public static LazyStaticSingleton getInstance() {
        if (singleton == null) {
            synchronized (LazyStaticSingleton.class) {
                singleton = new LazyStaticSingleton();
            }
        }
        return singleton;
    }
}