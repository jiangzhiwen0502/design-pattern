package com.jzwj.designpattern.singleton.lazy;

/**
 * @ClassName LazySyncSingleton
 * @Description 同步方法饿汉式
 * 优点：使用同步方法，线程安全
 * 缺点：效率十分低下
 * @Author jiangZhiWen
 * @Date 2021/6/25 23:55
 * @Version 1.0
 */
public class LazySyncSingleton {
    private static LazySyncSingleton lazySingleton = null;

    private LazySyncSingleton() {
    }

    public synchronized static LazySyncSingleton getInstance() {
        if (null == lazySingleton) {
            lazySingleton = new LazySyncSingleton();
        }
        return lazySingleton;
    }
}