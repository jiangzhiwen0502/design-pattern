package com.jzwj.designpattern.singleton.hungry;

/**
 * @ClassName HungryStaticSingleton
 * @Description 静态代码块饿汉式：类实例化的过程放在了静态代码块中，在类装载的时候，就执行静态代码块中的代码，初始化类的实例。
 * 优点：写法简单，创建对象时没有加任何的锁、执行效率比较高，在类装载的时候就完成实例化，避免了线程安全问题。
 * 缺点：在类装载时完成实例化，没有达到Lazy Loading的效果。如果从始至终从未使用过这个实例，则会造成内存的浪费
 * @Author jiangZhiWen
 * @Date 2021/6/25 23:35
 * @Version 1.0
 */
public class HungryStaticSingleton {
    private static final HungryStaticSingleton hungrySigleton;

    static {
        hungrySigleton = new HungryStaticSingleton();
    }

    private HungryStaticSingleton() {
    }

    public static HungryStaticSingleton getInstance() {
        return hungrySigleton;
    }
}