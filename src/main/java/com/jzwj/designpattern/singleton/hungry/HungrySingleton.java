package com.jzwj.designpattern.singleton.hungry;

/**
 * @ClassName HungrySingleton
 * @Description 静态常量饿汉式
 * 优点：写法简单，在类装载的时候就完成实例化，避免了线程安全问题。
 * 缺点：在类装载时完成实例化，没有达到Lazy Loading的效果。如果从始至终从未使用过这个实例，则会造成内存的浪费
 * @Author jiangZhiWen
 * @Date 2021/6/25 23:20
 * @Version 1.0
 */
public class HungrySingleton {
    private static final HungrySingleton hungrySigleton = new HungrySingleton();

    private HungrySingleton() {
    }

    public static HungrySingleton getInstance() {
        return hungrySigleton;
    }
}
